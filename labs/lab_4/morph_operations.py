import cv2
import numpy as np
from labs.help.image_utils import get_file_info, draw_image, load_image, draw_images
from copy import deepcopy

DEFAULT_RESULTS_FOLDER = "results/"

BLACK_COLOR = 0
WHITE_COLOR = 255


def cv_thresholding(img):
    ret, thresh1 = cv2.threshold(img, 127, WHITE_COLOR, cv2.THRESH_BINARY)
    ret, thresh2 = cv2.threshold(img, 127, WHITE_COLOR, cv2.THRESH_BINARY_INV)
    ret, thresh3 = cv2.threshold(img, 127, WHITE_COLOR, cv2.THRESH_TRUNC)
    ret, thresh4 = cv2.threshold(img, 127, WHITE_COLOR, cv2.THRESH_TOZERO)
    ret, thresh5 = cv2.threshold(img, 127, WHITE_COLOR, cv2.THRESH_TOZERO_INV)

    titles = ['Original Image', 'BINARY', 'BINARY_INV', 'TRUNC', 'TOZERO', 'TOZERO_INV']
    images = [img, thresh1, thresh2, thresh3, thresh4, thresh5]

    draw_images(images, titles)


def cv_adaptive_threshold(img):
    ret, th1 = cv2.threshold(img, 127, WHITE_COLOR, cv2.THRESH_BINARY)
    th2 = cv2.adaptiveThreshold(img, WHITE_COLOR, cv2.ADAPTIVE_THRESH_MEAN_C,
                                cv2.THRESH_BINARY, 11, 2)
    th3 = cv2.adaptiveThreshold(img, WHITE_COLOR, cv2.ADAPTIVE_THRESH_GAUSSIAN_C,
                                cv2.THRESH_BINARY, 11, 2)

    titles = ['Original Image', 'Global Thresholding (v = 127)',
              'Adaptive Mean Thresholding', 'Adaptive Gaussian Thresholding']
    images = [img, th1, th2, th3]
    draw_images(images, titles)


def cv_erode(img, mask_size):
    kernel = np.ones((mask_size, mask_size), np.uint8)
    erosion = cv2.erode(img, kernel, iterations=1)
    return erosion


def cv_dilation(img, mask_size):
    kernel = np.ones((mask_size, mask_size), np.uint8)
    dilation = cv2.dilate(img, kernel, iterations=1)
    return dilation


def cv_open(img, mask_size):
    kernel = np.ones((mask_size, mask_size), np.uint8)
    opening = cv2.morphologyEx(img, cv2.MORPH_OPEN, kernel)
    return opening


def cv_close(img, mask_size):
    kernel = np.ones((mask_size, mask_size), np.uint8)
    closing = cv2.morphologyEx(img, cv2.MORPH_CLOSE, kernel)
    return closing


def simple_binarization(img, threshold=127):
    new_img = deepcopy(img)
    img_height, img_width = img.shape

    for i in range(0, img_height):
        for j in range(0, img_width):
            new_img[i][j] = WHITE_COLOR if new_img[i][j] > threshold else 0
    return new_img


def create_mask(mask_size=3):
    assert (mask_size > 2)
    assert (mask_size % 2 != 0)
    mask = np.full(
        shape=(mask_size, mask_size),
        fill_value=WHITE_COLOR,
        dtype=np.uint8)
    return mask


def dilation(img, mask):
    img_height, img_width = img.shape
    mask_size = mask.shape[0]
    mask_size_half = mask_size // 2
    img_dilation = deepcopy(img)
    target_pixels = zip(*np.where(img == WHITE_COLOR))
    for row, col in target_pixels:
        has_anchor = False
        for i in range(mask_size):
            for j in range(mask_size):
                current_row = row + i - mask_size_half
                current_col = col + j - mask_size_half
                if 0 <= current_row < img_height and 0 <= current_col < img_width:
                    # пересечение
                    img_dilation[current_row][current_col] = mask[i][j]
                    has_anchor = True
            if has_anchor:
                continue

    return img_dilation


def erosion(img, mask):
    img_height, img_width = img.shape
    mask_size = mask.shape[0]
    mask_size_half = mask_size // 2
    img_erosion = deepcopy(img)
    target_pixels = zip(*np.where(img == WHITE_COLOR))
    for row, col in target_pixels:
        for i in range(mask_size):
            for j in range(mask_size):
                current_row = row + i - mask_size_half
                current_col = col + j - mask_size_half
                # если B не является подмножеством A, то точка равна 0
                if 0 <= current_row < img_height and 0 <= current_col < img_width:
                    if img[current_row][current_col] == mask[i][j]:
                        continue
                img_erosion[row][col] = BLACK_COLOR

    return img_erosion


def morph_opening(img, mask):
    temp_erosion = erosion(img, mask)
    img_opening = dilation(temp_erosion, mask)
    return img_opening


def morph_closing(img, mask):
    temp_dilation = dilation(img, mask)
    img_closing = erosion(temp_dilation, mask)
    return img_closing


def binarization_analyze(img, mask_sizes):
    binarized_image = simple_binarization(img)
    draw_image(binarized_image, DEFAULT_RESULTS_FOLDER, "binarized image", False, True)

    # --- compare ---
    simple_thresholds = [32, 64, 112, 128, 144, 192, 224]
    simple_binarized_images = [simple_binarization(img, threshold) for threshold in simple_thresholds]
    simple_binarized_titles = [f"threshold {threshold}" for threshold in simple_thresholds]
    draw_images(simple_binarized_images, simple_binarized_titles)

    for mask_size in mask_sizes:
        print(f"Mask size ({mask_size}x{mask_size})")
        mask = create_mask(mask_size)

        img_dilation = dilation(binarized_image, mask)
        img_erosion = erosion(binarized_image, mask)
        img_opening = morph_opening(binarized_image, mask)
        img_closing = morph_closing(binarized_image, mask)

        images = [img_dilation, img_erosion, img_opening, img_closing]
        titles = ["dilation", "erosion", "opening", "closing"]
        draw_images(images, titles, columns=2)


def example_show(img):
    # ----- simple binarization -----
    binarized_image = simple_binarization(img)
    draw_image(binarized_image, DEFAULT_RESULTS_FOLDER, "binarized image", False, True)

    mask_size = 7
    mask = create_mask(mask_size)

    # ----- dilation -----
    img_dilation = dilation(binarized_image, mask)
    draw_image(img_dilation, DEFAULT_RESULTS_FOLDER, f"dilation (mask {mask_size}x{mask_size})", False, True)

    # ----- erosion -----
    img_erosion = erosion(binarized_image, mask)
    draw_image(img_erosion, DEFAULT_RESULTS_FOLDER, f"erosion (mask {mask_size}x{mask_size})", False, True)

    # ----- morphological opening -----
    img_opening = morph_opening(binarized_image, mask)
    draw_image(img_opening, DEFAULT_RESULTS_FOLDER, f"opening (mask {mask_size}x{mask_size})", False, True)

    # ----- morphological closing -----
    img_closing = morph_closing(binarized_image, mask)
    draw_image(img_closing, DEFAULT_RESULTS_FOLDER, f"closing (mask {mask_size}x{mask_size})", False, True)

    # ----- cv2 built-in methods -----
    # cv_thresholding(base_image)
    # cv_adaptive_threshold(base_image)


def main():
    fullname = f"images/building.png"
    filename, file_extension, *_ = get_file_info(fullname)

    base_image = load_image(fullname)
    draw_image(base_image, DEFAULT_RESULTS_FOLDER, "base image", False, True)

    mask_sizes = [3, 5, 7]
    binarization_analyze(base_image, mask_sizes)


if __name__ == '__main__':
    main()
