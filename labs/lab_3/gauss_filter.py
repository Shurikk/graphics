import numpy as np
import cv2
from math import ceil, pi
from labs.lab_3.image_utils import add_noise, calc_diff, color_renormalize
from labs.help.image_utils import draw_image, load_image, filename_format, get_file_info, draw_images

DEFAULT_RESULTS_FOLDER = "gauss_results/"


def gauss_filter(image, ksize=3, sigma=10):
    kernel = cv2.getGaussianKernel(ksize ** 2, sigma, cv2.CV_32F).reshape(ksize, ksize)
    # Рассчет значения, насколько нужно увечилить кол-во пикселей изображения
    img_h, img_w = image.shape
    flt_h, flt_w = kernel.shape

    p_h = ceil((flt_h - 1) / 2)
    p_w = ceil((flt_w - 1) / 2)
    # создаем новое изображение увеличенное по всем сторонам на padding размер
    # заполняем его нулями
    padding = np.zeros((img_h + (2 * p_h), img_w + (2 * p_w)))
    # присваиваем исх. изображение внутрь padding
    padding[p_h:padding.shape[0] - p_h, p_w:padding.shape[1] - p_w] = image
    result = np.zeros(image.shape)
    # рассчет свертки ядра для изображения
    for r in range(img_h):
        for c in range(img_w):
            result[r, c] = np.sum(kernel * padding[r:r + flt_h, c:c + flt_w])
    normalized_result = color_renormalize(result)
    return normalized_result


def gauss_optimizer(image, noise, sigmas, iterations=3):
    results = dict()
    images = list()
    for i in range(iterations):
        image_with_noise = add_noise(base_image, var=noise)
        for sigma in sigmas:
            gauss_image = gauss_filter(image_with_noise, sigma=sigma)
            diff = calc_diff(image, gauss_image)
            if sigma not in results.keys():
                results[sigma] = diff
                images.append(gauss_image)
            else:
                results[sigma] += diff

    for key, value in results.items():
        results[key] = results[key] / iterations

    titles = [f"sigma = {key}" for key, value in results.items()]
    draw_images(images, titles)
    return results


def example_test(image, noise, sigma):
    image_with_noise = add_noise(image, var=noise)

    draw_image(image_with_noise, DEFAULT_RESULTS_FOLDER,
               filename_format(filename, "Image_with_noise", file_extension), False, True)

    filtered_image = gauss_filter(image_with_noise, sigma=sigma)
    draw_image(filtered_image, DEFAULT_RESULTS_FOLDER,
               filename_format(filename, "Filtered_image", file_extension), False, True)

    difference = calc_diff(image, filtered_image)
    print(difference)


if __name__ == '__main__':
    fullname = f"images/building.png"
    filename, file_extension, *_ = get_file_info(fullname)

    base_image = load_image(fullname)

    image_noise = 10
    sigmas_to_analyze = range(2, 20, 2)

    gauss_sigma_results = gauss_optimizer(base_image, image_noise, sigmas_to_analyze)
    best_sigma = min(gauss_sigma_results, key=gauss_sigma_results.get)
    print(gauss_sigma_results)
    print(f"Best sigma: {best_sigma} with {gauss_sigma_results[best_sigma]} difference")

    example_test(base_image, noise=image_noise, sigma=best_sigma)
