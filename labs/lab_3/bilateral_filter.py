import numpy as np
from labs.lab_3.image_utils import add_noise, calc_diff
from labs.help.image_utils import draw_image, load_image, filename_format, get_file_info, draw_images

DEFAULT_RESULTS_FOLDER = "bilateral_results/"


def distance(x, y, i, j):
    return np.sqrt((x - i) ** 2 + (y - j) ** 2)


def gaussian(x, sigma):
    return (1.0 / (2 * np.pi * (sigma ** 2))) * np.exp(- (x ** 2) / (2 * sigma ** 2))


def apply_bilateral_filter(source, filtered_image, x, y, diameter, sigma_i, sigma_s):
    hl = int(diameter / 2)
    i_filtered = 0
    Wp = 0
    i = 0
    while i < diameter:
        j = 0
        while j < diameter:
            neighbour_x = max(0, min(len(source) - 1, x - (hl - i)))
            neighbour_y = max(0, min(len(source[0]) - 1, y - (hl - j)))

            gi = gaussian(source[neighbour_x, neighbour_y] - source[x, y], sigma_i)
            gs = gaussian(distance(neighbour_x, neighbour_y, x, y), sigma_s)
            w = gi * gs
            i_filtered += source[neighbour_x, neighbour_y] * w
            Wp += w
            j += 1
        i += 1
    i_filtered = i_filtered / Wp
    filtered_image[x, y] = int(round(i_filtered))


def bilateral_filter_own(source, filter_diameter, sigma_i, sigma_s):
    filtered_image = np.zeros(source.shape)

    i = 0
    while i < len(source):
        j = 0
        while j < len(source[0]):
            apply_bilateral_filter(source, filtered_image, i, j, filter_diameter, sigma_i, sigma_s)
            j += 1
        i += 1
    return filtered_image


def bilateral_optimizer(image, noise, i_sigmas, s_sigmas):
    image_with_noise = add_noise(image, var=noise)
    draw_image(image_with_noise, DEFAULT_RESULTS_FOLDER, "Image_with_noise", False, True)

    results = dict()
    s_titles = [f"sigma = {s_sigma}" for s_sigma in s_sigmas]
    for i_sigma in i_sigmas:
        i_images = list()
        print(f"=============== Sigma intensity {i_sigma} =============== ")
        for s_sigma in s_sigmas:
            filtered_image = bilateral_filter_own(image_with_noise, 5, i_sigma, s_sigma)
            i_images.append(filtered_image)
            difference = calc_diff(image, filtered_image)
            results[(i_sigma, s_sigma)] = difference
            print(f"Sigma i ({i_sigma}) s ({s_sigma}) = {difference}")
        draw_images(i_images, s_titles, columns=2)
    best_sigma = min(results, key=results.get)
    print(results)
    print(f"Best sigma: {best_sigma} with {results[best_sigma]} difference")


def main():
    fullname = f"images/building.png"
    filename, file_extension, *_ = get_file_info(fullname)

    base_image = load_image(fullname)
    draw_image(base_image, DEFAULT_RESULTS_FOLDER,
               filename_format(filename, "base_image", file_extension), False, True)

    image_noise = 10
    i_sigmas = [32, 48, 64]
    s_sigmas = [8, 16, 32, 64]
    bilateral_optimizer(base_image, image_noise, i_sigmas, s_sigmas)

    # image_with_noise = add_noise(base_image, var=image_noise)
    # draw_image(image_with_noise,  DEFAULT_RESULTS_FOLDER,
    #            filename_format(filename, "Image_with_noise", file_extension), False, True)
    #
    # filtered_image = bilateral_filter_own(image_with_noise, 5, 12.0, 16.0)
    # draw_image(filtered_image, DEFAULT_RESULTS_FOLDER,
    #            filename_format(filename, "Filtered_Image", file_extension), False, True)
    #
    # difference = calc_diff(base_image, filtered_image)
    # print(difference)


if __name__ == '__main__':
    main()
