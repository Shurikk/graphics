import numpy as np


def add_noise(image, mean=0, var=10):
    noise = np.random.normal(mean, var, image.shape)
    output = image + noise
    output[output > 255] = 255
    output[output < 0] = 0
    return output


def color_renormalize(img):
    re_norm = img.copy()
    re_norm *= 255.0 / re_norm.max()
    re_norm = np.uint8(re_norm)
    return re_norm


def calc_diff(img1, img2):
    rows, cols = img1.shape
    square = rows * cols
    temp_img1 = np.float32(img1.copy())
    temp_img2 = np.float32(img2.copy())
    result = np.sum((np.abs(temp_img1 - temp_img2))) / square
    return result
