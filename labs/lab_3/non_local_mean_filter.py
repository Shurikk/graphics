from copy import deepcopy

import numpy as np
from labs.lab_3.image_utils import add_noise, calc_diff
from labs.help.image_utils import draw_image, load_image, filename_format, get_file_info, draw_images

DEFAULT_RESULTS_FOLDER = "non_local_mean/"


def non_local_mean(image, sigma, patch_big=7, patch_small=3):
    pad = patch_big + patch_small

    # img = np.pad(image, pad, mode='reflect')
    result_image = np.zeros(image.shape)
    height, width = image.shape

    H_gauss = 25

    img = np.zeros((height + (2 * pad), width + (2 * pad)))
    img[pad:img.shape[0] - pad, pad: img.shape[1] - pad] = image

    for y in range(pad, height + pad):
        for x in range(pad, width + pad):
            current_val = 0
            start_y = y - patch_big
            start_x = x - patch_big
            end_y = y + patch_big
            end_x = x + patch_big

            wp, maxweight = 0, 0

            for ypix in range(start_y, end_y):
                for xpix in range(start_x, end_x):
                    window1 = img[y - patch_small: y + patch_small, x - patch_small: x + patch_small].copy()
                    windows2 = img[ypix - patch_small:ypix + patch_small, xpix - patch_small:xpix + patch_small].copy()
                    weight = np.exp(-(np.sum((window1 - windows2) ** 2) + 2 * (sigma ** 2)) / (H_gauss ** 2))
                    if weight > maxweight:
                        maxweight = weight

                    if (y == ypix) and (x == xpix):
                        weight = maxweight

                    wp += weight
                    current_val += weight * img[ypix, xpix]
            result_image[y - pad, x - pad] = current_val / wp
            if result_image[y - pad, x - pad] > 255:
                result_image[y - pad, x - pad] = 255
            if result_image[y - pad, x - pad] < 0:
                result_image[y - pad, x - pad] = 0
            # print(result_image[y - pad, x - pad])
    return result_image


def nlm_optimizer(image, noise, sigmas):
    image_with_noise = add_noise(image, var=noise)
    draw_image(image_with_noise, DEFAULT_RESULTS_FOLDER, "Image with noise", False, True)

    results = dict()
    images = list()
    titles = [f"h = {sigma}" for sigma in sigmas]
    for sigma in sigmas:
        img = deepcopy(image_with_noise)
        filtered_image = non_local_mean(img, sigma)
        images.append(filtered_image)
        difference = calc_diff(image, filtered_image)
        results[sigma] = difference
        print(f"H ({sigma}) = {difference}")
    draw_images(images, titles, columns=2)
    best_sigma = min(results, key=results.get)
    print(results)
    print(f"Best sigma: {best_sigma} with {results[best_sigma]} difference")


def main():
    fullname = f"images/building.png"
    filename, file_extension, *_ = get_file_info(fullname)

    base_image = load_image(fullname)
    draw_image(base_image, DEFAULT_RESULTS_FOLDER,
               filename_format(filename, "base_image", file_extension), False, True)

    image_noise = 10
    sigmas = [5, 10, 20, 40]
    nlm_optimizer(base_image, image_noise, sigmas)

    # image_with_noise = add_noise(base_image, var=image_noise)
    # draw_image(image_with_noise, DEFAULT_RESULTS_FOLDER,
    #            filename_format(filename, "Image_with_noise", file_extension), False, True)
    #
    # filtered_image = non_local_mean(image_with_noise, 10)
    # draw_image(filtered_image, DEFAULT_RESULTS_FOLDER,
    #            filename_format(filename, "Filtered_Image", file_extension), False, True)
    #
    # difference = calc_diff(base_image, filtered_image)
    # print(difference)


if __name__ == '__main__':
    main()
