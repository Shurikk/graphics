from typing import Tuple

from PIL import Image

from labs.lab_1.image_help import load_grey_image, analyze_histogram, compare_histograms

MIN_HIST_BORDER = 0
MAX_HIST_BORDER = 255

CUT_PERCENT = 0.05


def find_histogram_borders(image: Image) -> Tuple[int, int]:
    print("Расчет левой и правой границы исходной гистограммы")
    a, b = MIN_HIST_BORDER, MAX_HIST_BORDER  # search start histogram borders
    histogram = image.histogram()
    image_size = image.width * image.height
    cut_image_size = round(image_size * CUT_PERCENT)
    print(f"Размер изображения: {image_size}")
    print(f"Заданное отсечение размера изображения: {cut_image_size} (5%)")
    pixel_amount = 0

    while pixel_amount <= cut_image_size:
        if histogram[a] < histogram[b]:
            if pixel_amount + histogram[a] <= cut_image_size:
                pixel_amount += histogram[a]
                a += 1
            else:
                break
        else:
            if pixel_amount + histogram[b] <= cut_image_size:
                pixel_amount += histogram[b]
                b -= 1
            else:
                break

    print(f"Фактическое отсечение размера изображения: {pixel_amount} ({pixel_amount / image_size * 100:.2f}%)")
    print(f"Левая граница: {a}")
    print(f"Правая граница: {b}")
    return a, b


def lineal_recalculate_image(image: Image, a: int, b: int):
    print("Расчет нового изображения по таблице переходов (линейное растяжение)")
    c, d = MIN_HIST_BORDER, MAX_HIST_BORDER  # new histogram borders
    for i in range(image.width):
        for j in range(image.height):
            pixel_index = (i, j)
            # grey intensity formula
            updated_pixel = round((image.getpixel(pixel_index) - a) * ((d - c) / (b - a)) + c)
            image.putpixel(pixel_index, updated_pixel)


if __name__ == '__main__':
    print("Задание 1: Линейное растяжение гистограммы")
    fullname = "images/road.jpg"
    filename = fullname.split(".")[0]
    extension = fullname.split(".")[1]
    base_image = load_grey_image(fullname)
    base_image.save(f"{filename}_grey.{extension}")

    base_histogram = base_image.histogram().copy()
    analyze_histogram(base_histogram, "Исходная гистограмма")

    base_a, base_b = find_histogram_borders(base_image)
    lineal_recalculate_image(base_image, base_a, base_b)

    recalculated_histogram = base_image.histogram()
    analyze_histogram(recalculated_histogram, "Линейное растяжение гистограммы")
    compare_histograms(base_histogram, recalculated_histogram)
    base_image.save(f"{filename}_grey_result_1.{extension}")
