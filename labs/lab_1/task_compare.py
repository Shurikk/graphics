from labs.lab_1.image_help import load_grey_image, compare_histograms
from labs.lab_1.task_1_lineal import find_histogram_borders, lineal_recalculate_image
from labs.lab_1.task_2_equal import get_normalised_histogram, get_cumulative_histogram, equal_recalculate_image
from labs.help.help_functions import print_separator

if __name__ == '__main__':
    print("Сравнение методов")
    fullname = 'images/space.jpg'
    filename = fullname.split(".")[0]
    extension = fullname.split(".")[1]
    base_image = load_grey_image(fullname)
    base_image.save(f"{filename}_grey.{extension}")

    base_histogram = base_image.histogram().copy()

    base_a, base_b = find_histogram_borders(base_image)
    lineal_recalculate_image(base_image, base_a, base_b)

    lineal_histogram = base_image.histogram()
    base_image.save(f"{filename}_grey_result_1.{extension}")

    print_separator()
    base_image = load_grey_image(fullname)
    normalised_histogram = get_normalised_histogram(base_image)
    cumulative_histogram = get_cumulative_histogram(normalised_histogram)

    equal_recalculate_image(base_image, cumulative_histogram)

    equal_histogram = base_image.histogram()
    base_image.save(f"{filename}_grey_result_2.{extension}")

    compare_histograms(base_histogram, lineal_histogram, equal_histogram)
