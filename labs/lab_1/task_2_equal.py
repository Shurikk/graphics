from PIL import Image

from labs.lab_1.image_help import load_grey_image, analyze_histogram, Histogram, compare_histograms

MAX_HIST_BORDER = 255

def get_normalised_histogram(image: Image) -> Histogram:
    print("Нормализация гистограмы")
    histogram = image.histogram()
    image_size = image.width * image.height
    return [x / image_size for x in histogram]


def get_cumulative_histogram(norm_histogram: Histogram) -> Histogram:
    print("Расчет кумулятивной гистограмы")
    for i in range(1, len(norm_histogram)):
        norm_histogram[i] += norm_histogram[i - 1]
    return norm_histogram


def equal_recalculate_image(image: Image, histogram: Histogram):
    print("Расчет нового изображения по таблице переходов (кумулятивной гистограме)")
    result = image.getdata()
    for i in range(image.width):
        for j in range(image.height):
            pixel_index = (i, j)
            image.putpixel(pixel_index, round(histogram[image.getpixel(pixel_index)] * MAX_HIST_BORDER))


if __name__ == '__main__':
    print("Задание 2: Эквализация гистограммы")
    fullname = 'images/city2.jpg'
    filename = fullname.split(".")[0]
    extension = fullname.split(".")[1]
    base_image = load_grey_image(fullname)
    base_image.save(f"{filename}_grey.{extension}")

    base_histogram = base_image.histogram()
    analyze_histogram(base_histogram, "Исходная гистограмма")

    normalised_histogram = get_normalised_histogram(base_image)
    cumulative_histogram = get_cumulative_histogram(normalised_histogram)

    equal_recalculate_image(base_image, cumulative_histogram)

    recalculated_histogram = base_image.histogram()
    analyze_histogram(recalculated_histogram, "Эквализационная гистограмма")
    compare_histograms(base_histogram, recalculated_histogram)

    normalised_new_histogram = get_normalised_histogram(base_image)
    cumulative_new_histogram = get_cumulative_histogram(normalised_new_histogram)

    compare_histograms(cumulative_histogram, cumulative_new_histogram)

    base_image.save(f"{filename}_grey_result_2.{extension}")
