from typing import List

import matplotlib.pyplot as plt
import numpy as np
from PIL import Image

Histogram = List[float]


def load_grey_image(image_filename: str) -> Image:
    print("Загрузка картинки и перевод ее в серый цвет")
    return Image.open(image_filename).convert('L')  # open image and convert to gray


def analyze_histogram(histogram: Histogram, plot_title: str = "Гистограмма"):
    plt.bar(np.arange(len(histogram)), histogram)
    plt.title(plot_title)
    plt.show()


def compare_histograms(*histograms: Histogram, plot_title: str = "Сравнение гистограмм"):
    for histogram in histograms:
        plt.bar(np.arange(len(histogram)), histogram)
    plt.title(plot_title)
    plt.show()
