import cv2
import numpy as np
from copy import deepcopy

from labs.course_work.Segment import Segment
from labs.help.image_utils import get_file_info, load_image, draw_image, draw_histogram

COUNT_OF_COLORS = 256

draw_hist = False


def get_clip_limit(constant, x_size, y_size):
    return constant * x_size * y_size / COUNT_OF_COLORS


def get_adaptive_clip_limit(hist, constant=4):
    return int((np.max(hist) + np.mean(hist)) / constant)


def get_hist(image):
    hist = cv2.calcHist([image], [0], None, [256], [0, 256])
    return [int(x[0]) for x in hist]


def place_pixels(limited_hist, additional_pixels, limit):
    while additional_pixels > 0:
        val, idx = min((val, idx) for (idx, val) in enumerate(limited_hist))
        if val < limit:
            limited_hist[idx] += 1
            # additional_pixels -= 1
        else:
            break


def round_pixels(limited_hist, limit):
    limited_hist = [limit] * len(limited_hist)
    return limited_hist


def process_additional_pixels(limited_hist, additional_pixels, limit):
    i = 0
    has_changed = False
    while additional_pixels > 0:
        if limited_hist[i] < limit:
            limited_hist[i] += 1
            additional_pixels -= 1
            has_changed = True

        if i < len(limited_hist) - 1:
            i += 1
        else:
            if has_changed:
                i = 0
                has_changed = False
            else:
                break


def local_place_pixels(limited_hist, index, additional_pixels, limit):
    left_i = index - 1
    right_i = index + 1
    pixels = additional_pixels
    while pixels > 0 and (left_i >= 0 or right_i < len(limited_hist)):
        if left_i >= 0 and limited_hist[left_i] < limit:
            limited_hist[left_i] += 1
            pixels -= 1
        left_i = left_i - 1
        if right_i < len(limited_hist) and limited_hist[right_i] < limit and pixels > 0:
            limited_hist[right_i] += 1
            pixels -= 1
        right_i = right_i + 1


def distribute_pixels(limited_hist, additional_pixels, limit):
    pixel_amount = additional_pixels
    a = 0
    b = len(limited_hist) - 1
    while pixel_amount > 0:
        if limited_hist[a] < limited_hist[b]:
            if limited_hist[a] + 1 < limit:
                limited_hist[a] += 1
                pixel_amount -= 1
                a += 1
            else:
                break
        else:
            if limited_hist[b] + 1 < limit:
                limited_hist[b] += 1
                pixel_amount -= 1
                b -= 1
            else:
                break


def get_segment_histogram(image, segment):
    hist = np.zeros((COUNT_OF_COLORS,), dtype=int)
    print(f"segment: {segment.top} {segment.bottom} {segment.left} {segment.right}")
    for x in range(segment.top, segment.bottom):
        for y in range(segment.left, segment.right):
            hist[image[x, y]] += 1
    return hist


def get_limited_histogram(hist, limit):
    limited_hist = deepcopy(hist)
    additional_pixels = 0

    for i, p_color in enumerate(limited_hist):
        delta = p_color - limit
        if delta > 0:
            additional_pixels += delta
            limited_hist[i] = limit
            # local_place_pixels(limited_hist, i, delta, limit)

    process_additional_pixels(limited_hist, additional_pixels, limit)
    # place_pixels(limited_hist, additional_pixels, limit)
    # distribute_pixels(limited_hist, additional_pixels, limit)
    # limited_hist = round_pixels(limited_hist, limit)

    return limited_hist


def get_normalised_histogram(hist, size):
    return [x / size for x in hist]


def get_cumulative_histogram(hist):
    cumulative_hist = deepcopy(hist)
    for i in range(1, len(cumulative_hist)):
        cumulative_hist[i] += cumulative_hist[i - 1]
    return cumulative_hist


def create_image(old_image, segment, hist, limit=40):
    new_image = np.zeros(old_image.shape)
    for i in range(segment.top, segment.bottom):
        for j in range(segment.left, segment.right):
            image_by_hist_value = hist[old_image[i, j]] * (COUNT_OF_COLORS - 1)
            old_image_value = old_image[i, j]
            new_image[i, j] = max(old_image_value - limit, min(old_image_value + limit, image_by_hist_value))

    for i in range(segment.top, segment.bottom):
        new_image[i, segment.left] = get_bilinear_pixel(old_image, segment.left, i)
        new_image[i, segment.right] = get_bilinear_pixel(old_image, segment.right, i)

    for i in range(segment.left, segment.right):
        new_image[segment.bottom, i] = get_bilinear_pixel(old_image, i, segment.bottom)
        new_image[segment.top, i] = get_bilinear_pixel(old_image, i, segment.top)

    return new_image


def get_image_size(image):
    x_size, y_size = image.shape
    return x_size * y_size


def get_hist_size(hist):
    return len(hist)


def process_segment(image, segment, limit, use_adaptive_limit=False):
    hist = get_segment_histogram(image, segment)

    limit = max(1, get_adaptive_clip_limit(hist)) if use_adaptive_limit else limit
    print(limit)

    limited_hist = get_limited_histogram(hist, limit)
    normalized_hist = get_normalised_histogram(limited_hist, segment.get_size())
    cumulative_hist = get_cumulative_histogram(normalized_hist)
    new_image_segment = create_image(image, segment, cumulative_hist)  # limit

    # draw_histogram(hist)
    # draw_histogram(limited_hist)
    # draw_histogram(normalized_hist)
    # draw_histogram(cumulative_hist)
    # exit(0)

    return new_image_segment


def process_image(image, segment_size, clip_limit, use_adaptive_limit):
    image_height, image_width = image.shape
    segment = Segment(image, segment_size)

    result_image = np.zeros(image.shape)

    while segment.bottom < image_height:
        segment.reset_horizontal()
        while segment.right < image_width:
            result_image += process_segment(image, segment, clip_limit, use_adaptive_limit)
            segment.shift_right()
        segment.shift_down()
    return result_image


def bilinear_interpolate_image(image):
    bilinear_img = cv2.resize(image, None, fx=10, fy=10, interpolation=cv2.INTER_LINEAR)
    return bilinear_img


def bicubic_interpolate_image(image):
    bicubic_img = cv2.resize(image, None, fx=10, fy=10, interpolation=cv2.INTER_CUBIC)
    return bicubic_img


# v(x,y) = ax + by + cxy + d
def interpolate_img(img):
    result = img.copy()
    img_height, img_width = img.shape
    num_of_pix_handled = 0

    for i in range(1, img_height - 1):
        for j in range(1, img_width - 1):
            M1 = np.array([[j, i - 1, (i - 1) * j, 1],
                           [j + 1, i, i * (j + 1), 1],
                           [j, i + 1, (i + 1) * j, 1],
                           [j - 1, i, i * (j - 1), 1]])
            v1 = np.array([img[i - 1, j],
                           img[i, j + 1],
                           img[i + 1, j],
                           img[i, j - 1]])
            arr = np.linalg.lstsq(M1, v1)
            result[i, j] = int(arr[0][0] * j + arr[0][1] * i + arr[0][2] * i * j + arr[0][3])
        num_of_pix_handled += img_width
        # print(int(num_of_pix_handled / (img_height * img_width / 100)))

    return result


def get_bilinear_pixel(imArr, posX, posY):
    out = []
    modXi = int(posX)
    modYi = int(posY)
    modXf = posX - modXi
    modYf = posY - modYi
    modXiPlusOneLim = min(modXi + 1, imArr.shape[1] - 1)
    modYiPlusOneLim = min(modYi + 1, imArr.shape[0] - 1)

    bl = imArr[modYi, modXi]
    br = imArr[modYi, modXiPlusOneLim]
    tl = imArr[modYiPlusOneLim, modXi]
    tr = imArr[modYiPlusOneLim, modXiPlusOneLim]

    b = modXf * br + (1. - modXf) * bl
    t = modXf * tr + (1. - modXf) * tl
    pxf = modYf * t + (1. - modYf) * b
    out.append(int(pxf + 0.5))
    return out[0]


def analyze_image(base_image, segment_size, clip_limit, use_adaptive_limit):
    draw_image(base_image, "", "base_image", False, True)

    histogram = get_hist(base_image)
    draw_histogram(histogram)

    new_image = process_image(base_image, segment_size, clip_limit, use_adaptive_limit)
    draw_image(new_image, "", f"CLAHE_image_size_{segment_size}_limit_{clip_limit}", False, True)

    interpolated_image = bilinear_interpolate_image(new_image)
    draw_image(interpolated_image, "", f"CLAHE_interpolated_image_{segment_size}_limit_{clip_limit}", False, True)

    interpolated_image = bicubic_interpolate_image(new_image)
    draw_image(interpolated_image, "", f"CLAHE_interpolated_image_{segment_size}_limit_{clip_limit}", False, True)

    interpolated_image = interpolate_img(new_image)
    draw_image(interpolated_image, "", f"CLAHE_interpolated_image_{segment_size}_limit_{clip_limit}", False, True)

    # interpolatedx2_image = interpolated_image
    # for i in range(0, interpolated_image.shape[0]):
    #     for j in range(0, interpolated_image.shape[1]):
    #         interpolatedx2_image[i, j] = GetBilinearPixel(interpolated_image, j, i)
    #
    # draw_image(interpolatedx2_image, "", f"CLAHE_interpolated_image_{segment_size}_limit_{clip_limit}", False, True)


def compare_image(base_image):
    segments_sizes = [4, 8, 16, 32, 64]
    clip_limits = [1, 2, 3]  # [1, 2, 4, 8, 16, 32, 64]
    use_adaptive_limit = False

    for segment_size in segments_sizes:
        for clip_limit in clip_limits:
            analyze_image(base_image, segment_size, clip_limit, use_adaptive_limit)


def main():
    segment_size = 8  # size of grid for histogram equalization
    clip_limit = 1  # clipLimit -> Threshold for contrast limiting
    use_adaptive_limit = False

    fullname = f"images/road.jpg"
    filename, file_extension, *_ = get_file_info(fullname)

    base_image = load_image(fullname)

    analyze_image(base_image, segment_size, clip_limit, use_adaptive_limit)
    # compare_image(base_image)


if __name__ == '__main__':
    main()
