class Segment:
    def __init__(self, image, segment_size):
        self.image_height, self.image_width = image.shape
        self.step = segment_size
        self.top = self.left = 0
        self.bottom = self.right = self.step - 1

    def shift_right(self):
        self.left = self.right
        new_right = self.right + self.step
        self.right = new_right if new_right < self.image_width or self.right == self.image_width - 1 else self.image_width - 1

    def shift_down(self):
        self.top = self.bottom
        new_bottom = self.bottom + self.step
        self.bottom = new_bottom if new_bottom < self.image_height or self.bottom == self.image_height - 1 else self.image_height - 1

    def reset_horizontal(self):
        self.left = 0
        self.right = self.step - 1

    def reset_vertical(self):
        self.top = 0
        self.bottom = self.step - 1

    def get_size(self):
        return (self.bottom - self.top + 1) * (self.right - self.left + 1)
