from labs.lab_2.edge_detector import EdgeDetector


class RobertsDetector(EdgeDetector):
    def __init__(self):
        super().__init__(f"{self.DEFAULT_RESULTS_FOLDER}roberts/")
        self.mask_x: EdgeDetector.Mask = [[1, 0], [0, -1]]
        self.mask_y: EdgeDetector.Mask = [[0, 1], [-1, 0]]

    def _convolution_fun(self, img, i, j, mask):
        return abs(img[i][j] * mask[0][0] +
                   img[i][j + 1] * mask[0][1] +
                   img[i + 1][j] * mask[1][0] +
                   img[i + 1][j + 1] * mask[1][1])

    def _addition_fun(self, first_img, second_img, i, j):
        return first_img[i][j] + second_img[i][j]


if __name__ == '__main__':
    print("Roberts operator")
    image_filename = "images/sot.png"
    RobertsDetector().execute(image_filename)
