from labs.lab_2.edge_detector import EdgeDetector


class PrevittDetector(EdgeDetector):
    def __init__(self):
        super().__init__(f"{self.DEFAULT_RESULTS_FOLDER}previtt/")
        self.mask_x: EdgeDetector.Mask = [[-1, 0, 1], [-1, 0, 1], [-1, 0, 1]]
        self.mask_y: EdgeDetector.Mask = [[-1, -1, -1], [0, 0, 0], [1, 1, 1]]

    def _convolution_fun(self, img, i, j, mask):
        return abs(img[i - 1][j - 1] * mask[0][0] +
                   img[i - 1][j] * mask[0][1] +
                   img[i - 1][j + 1] * mask[0][2] +
                   img[i][j - 1] * mask[1][0] +
                   img[i][j] * mask[1][1] +
                   img[i][j + 1] * mask[1][2] +
                   img[i + 1][j - 1] * mask[2][0] +
                   img[i + 1][j] * mask[2][1] +
                   img[i + 1][j + 1] * mask[2][2])

    def _addition_fun(self, first_img, second_img, i, j):
        return pow(pow(first_img[i][j], 2) + pow(second_img[i][j], 2), 0.5)


if __name__ == '__main__':
    print("Previtt operator")
    image_filename = "images/sot.png"
    PrevittDetector().execute(image_filename)
