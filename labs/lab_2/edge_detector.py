from typing import List

import cv2
import matplotlib.pyplot as plt
import numpy as np
from pathlib import Path

from labs.help.help_functions import benchmark
from labs.help.image_utils import load_image, filename_format, draw_image

"""
    Based on algorithms from:
    https://habr.com/ru/post/114452/
"""


class EdgeDetector:
    Mask = List[List[int]]
    DEFAULT_RESULTS_FOLDER = "results/"
    BASE_IMAGE_NAME_PART = "base_image"
    MASK_X_NAME_PART = "mask_x"
    MASK_Y_NAME_PART = "mask_y"
    FULL_MASK_NAME_PART = "full_mask"

    def __init__(self, results_folder=DEFAULT_RESULTS_FOLDER):
        self.mask_x: EdgeDetector.Mask = []
        self.mask_y: EdgeDetector.Mask = []
        self.results_folder = results_folder

    @benchmark(power=0)
    def execute(self, fullname, save=True, show=False):
        filename, extension, *_ = Path(fullname).name.split(".")[:2]
        base_image = load_image(fullname)

        draw_image(base_image, self.results_folder,
                   filename_format(filename, self.BASE_IMAGE_NAME_PART, extension), save, show)

        img_with_kx_mask = self.img_convolution(base_image, self.mask_x)
        draw_image(img_with_kx_mask, self.results_folder,
                   filename_format(filename, self.MASK_X_NAME_PART, extension), save, show)

        img_with_ky_mask = self.img_convolution(base_image, self.mask_y)
        draw_image(img_with_ky_mask, self.results_folder,
                   filename_format(filename, self.MASK_Y_NAME_PART, extension), save, show)

        addition_img = self.img_addition(img_with_kx_mask, img_with_ky_mask)
        draw_image(addition_img, self.results_folder,
                   filename_format(filename, self.FULL_MASK_NAME_PART, extension), save, show)
        return addition_img

    def img_convolution(self, img, mask):
        new_img = img.copy()
        img_height, img_width = new_img.shape

        for i in range(1, img_height - 2):
            for j in range(1, img_width - 2):
                new_img[i][j] = self._convolution_fun(img, i, j, mask)

        return new_img

    def img_addition(self, first_img, second_img):
        new_img = first_img.copy()
        img_height, img_width = first_img.shape

        for i in range(0, img_height):
            for j in range(0, img_width):
                new_img[i][j] = self._addition_fun(first_img, second_img, i, j)

        return new_img

    def _convolution_fun(self, img, i, j, mask):
        return 0

    def _addition_fun(self, first_img, second_img, i, j):
        return 0

    @staticmethod
    def compare(first_img, second_img):
        img_height, img_width = first_img.shape
        new_img = np.zeros((img_height, img_width))

        for i in range(0, img_height):
            for j in range(0, img_width):
                new_img[i][j] = abs(first_img[i][j] - second_img[i][j])

        return new_img

    # @staticmethod
    # def draw_image(img, saving_folder, saving_name, save, show):
    #     plt.tick_params(labelsize=0, length=0)
    #     plt.imshow(img, cmap='gray')
    #     if save:
    #         Path(saving_folder).mkdir(parents=True, exist_ok=True)
    #         plt.savefig(saving_folder + saving_name, bbox_inches='tight', pad_inches=0)
    #     if show:
    #         plt.show()
    # 
    # @staticmethod
    # def draw_graph(arr, x_label, y_label, saving_folder, saving_name, save, show):
    #     plt.bar(np.arange(len(arr)), arr)
    #     plt.grid(True)
    #     plt.xlabel(x_label)
    #     plt.ylabel(y_label)
    #     if save:
    #         Path(saving_folder).mkdir(parents=True, exist_ok=True)
    #         plt.savefig(saving_folder + saving_name, bbox_inches='tight', pad_inches=0)
    #     if show:
    #         plt.show()
    # 
    # @staticmethod
    # def _load_image(fullname):
    #     image = cv2.imread(fullname, cv2.IMREAD_GRAYSCALE).astype(np.int32)
    #     return image
    # 
    # @staticmethod
    # def _filename_format(filename, process_name, extension):
    #     return f"{filename}_{process_name}.{extension}"
