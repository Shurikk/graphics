from labs.lab_2.edge_detector import EdgeDetector
from labs.lab_2.previtt_detector import PrevittDetector
from labs.lab_2.roberts_detector import RobertsDetector
from labs.lab_2.sobel_detector import SobelDetector

COMPARE_FOLDER = "results/compare/"

if __name__ == '__main__':
    image_filename = "images/sot.png"
    print("Sobel operator")
    sobel_image = SobelDetector().execute(image_filename, save=False, show=False)
    print("Roberts operator")
    roberts_image = RobertsDetector().execute(image_filename, save=False, show=False)
    print("Previtt operator")
    previtt_image = PrevittDetector().execute(image_filename, save=False, show=False)

    sobel_roberts_compared = EdgeDetector.compare(sobel_image, roberts_image)
    EdgeDetector.draw_image(sobel_roberts_compared, COMPARE_FOLDER, "sobel_roberts", save=True, show=False)

    sobel_previtt_compared = EdgeDetector.compare(sobel_image, previtt_image)
    EdgeDetector.draw_image(sobel_previtt_compared, COMPARE_FOLDER, "sobel_previtt", save=True, show=False)

    roberts_previtt_compared = EdgeDetector.compare(roberts_image, previtt_image)
    EdgeDetector.draw_image(roberts_previtt_compared, COMPARE_FOLDER, "roberts_previtt", save=True, show=False)
