import cv2
import numpy as np
import matplotlib.pyplot as plt
from pathlib import Path
from PIL import Image


def get_file_info(filename):
    return Path(filename).name.split(".")[:2]


def filename_format(name, process_name, extension):
    return f"{name}_{process_name}.{extension}"


def load_image(path):
    image = cv2.imread(path, cv2.IMREAD_GRAYSCALE)
    return image


def load_grey_image(image_filename: str) -> Image:
    print("Загрузка картинки и перевод ее в серый цвет")
    return Image.open(image_filename).convert('L')  # open image and convert to gray


def analyze_histogram(histogram, plot_title: str = "Гистограмма"):
    plt.bar(np.arange(len(histogram)), histogram)
    plt.title(plot_title)
    plt.show()


def draw_image(img, saving_folder, saving_name, save, show):
    plt.tick_params(labelsize=0, length=0)
    plt.imshow(img, cmap='gray')
    plt.title(saving_name)
    if save:
        Path(saving_folder).mkdir(parents=True, exist_ok=True)
        plt.savefig(saving_folder + saving_name, bbox_inches='tight', pad_inches=0)
    if show:
        plt.show()


def draw_images(images, titles, columns=3, saving_folder="results", saving_name="Comparison", save=False, show=True):
    size = len(images)
    rows = int(np.ceil(size / columns))
    for i in range(size):
        plt.subplot(rows, columns, i + 1)
        plt.title(titles[i])
        plt.imshow(images[i], cmap='gray')
    if show:
        plt.show()
    if save:
        Path(saving_folder).mkdir(parents=True, exist_ok=True)
        plt.savefig(f"{saving_folder}/{saving_name}", bbox_inches='tight', pad_inches=0)


def draw_histogram(histogram, plot_title: str = "Гистограмма"):
    plt.bar(np.arange(len(histogram)), histogram)
    plt.title(plot_title)
    plt.show()


def compare_histograms(*histograms, plot_title: str = "Сравнение гистограмм"):
    for histogram in histograms:
        plt.bar(np.arange(len(histogram)), histogram)
    plt.title(plot_title)
    plt.show()
