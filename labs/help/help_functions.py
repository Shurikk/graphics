from time import time
from functools import wraps
from datetime import datetime

SEPARATOR_LEN = 60
SEPARATOR = '='

powers = {
    0: 'sec',
    3: 'ms',
    6: 'us',
    9: 'ns',
    12: 'ps'
}


def get_current_time():
    return datetime.today().strftime("%Y-%m-%d %H:%M:%S")


def print_separator(sep_len: int = SEPARATOR_LEN, sep: str = SEPARATOR, output_fun=print):
    """ Вывод разделителя """
    output_fun(sep * sep_len)


def print_header(text: str, header_len: int = SEPARATOR_LEN, sep: str = SEPARATOR, output_fun=print):
    """ Вывод форматированного заголовка """
    text_len = len(text)
    border_len = header_len - text_len - 2
    if border_len > 0:
        left_border_len = border_len // 2
        right_border_len = border_len - left_border_len
    else:
        left_border_len = right_border_len = 0
    header = sep * left_border_len + ' ' + text + ' ' + sep * right_border_len
    output_fun(header)


""" decorators """


def benchmark(iterations=1, power=9, total_time=None):
    def decorator(func):
        @wraps(func)
        def wrapper(*args, **kwargs):
            global f_res
            grade = pow(10, power)  # nano
            units = powers[power]
            average_time = 0.0
            for i in range(0, iterations):
                t0 = time()
                f_res = func(*args, **kwargs)
                t1 = time()
                average_time += (t1 - t0) * grade
            average_time /= float(iterations)
            if total_time is not None:
                total_time[0] += average_time
            print(f"Operation has taken\t{average_time:.4f} {units}\t")
            print_separator()
            return f_res

        return wrapper

    return decorator


def logger():
    def decorator(func):
        @wraps(func)
        def wrapper(*args, **kwargs):
            print('Function [%s] was started at %s' % (func.__name__, get_current_time()))
            res = func(*args, **kwargs)
            print('Function [%s] was ended at %s' % (func.__name__, get_current_time()))
            return res

        return wrapper

    return decorator
