import logging
import sys
from logging.handlers import TimedRotatingFileHandler

FORMATTER = logging.Formatter("%(asctime)s - %(name)s - %(levelname)s: %(message)s")
LOG_FILE = "my_app.log"


def get_console_handler(formatter):
    console_handler = logging.StreamHandler(sys.stdout)
    console_handler.setFormatter(formatter)
    return console_handler


def get_file_handler(log_file, formatter, time_handle, mode):
    if time_handle:
        file_handler = TimedRotatingFileHandler(log_file, when='midnight')
    else:
        file_handler = logging.FileHandler(log_file, mode=mode, encoding='utf-8')
    file_handler.setFormatter(formatter)
    return file_handler


def get_logger(logger_name,
               level=logging.DEBUG,
               console_formatter=FORMATTER,
               file_formatter=FORMATTER,
               log_file=LOG_FILE,
               file_mode='w',
               time_handle=False):
    logger = logging.getLogger(logger_name)
    logger.setLevel(level)
    logger.addHandler(get_console_handler(console_formatter))
    logger.addHandler(get_file_handler(log_file, file_formatter, time_handle, file_mode))
    logger.propagate = False
    return logger
